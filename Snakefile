#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

from pathlib import Path
import os, re
import subprocess

"""
	@uthor : Anaïs Louis
	Creation : April 2020
	Last update : August 2020 

	Langaha, a snakemake workflow to scaffold assemblies with reference genomes and/or separate haplotypes.
"""

configfile: "config.yaml"


# _________________________ PARAMETERS  _________________________ #

#chdir = os.getcwd()
asm = config['data']['asm'].split('/')[-1]
out_name = asm.rsplit('.', 1)[0]
wd = Path(config['paths']['output_dir']).resolve().as_posix()
path_to_sif = Path(config['paths']['singularity_imgs']).resolve().as_posix()

SCAFF_TOOL = []
if config['scaffolding']['ragoo'] :
	SCAFF_TOOL.append("ragoo")
if config['scaffolding']['ragtag'] :
	SCAFF_TOOL.append("ragtag")

#if config['phasing']['whatshap'] :
#	NB_HAPLOTYPES = list(str(range(1,int(config['whatshap']['ploidy']))))
NB_HAPLOTYPES = []
if config['phasing']['whatshap'] :
	for i in range(1,int(config['whatshap']['ploidy'])+1) :
		NB_HAPLOTYPES.append(str(i))



# _________________________ BEGIN RULES  _________________________ #

## END OF WORKFLOW ##
rule end:
	input:
		expand("{wd}/assembly_stats/{out_name}.{scaffolding}.fa.stats", wd=wd, scaffolding=SCAFF_TOOL, out_name=out_name),
		expand("{wd}/WhatsHap/{out}.whatshap.phased.vcf.gz", wd=wd, out=config['whatshap']['prefix']) if config['phasing']['whatshap'] else "",
		expand("{wd}/WhatsHap/Haplotypes/{prefix}.whatshap.haplotype_{nb}.fasta", wd=wd, prefix=config['whatshap']['prefix'], nb=NB_HAPLOTYPES) if config['phasing']['whatshap'] else "" 

## _______________________________ ##
## _________ Scaffolding _________ ##
## _______________________________ ##
## SCAFFOLDING WITH RAGOO ##
rule ragoo:
	input:
		assembly = config['data']['asm'],
		ref = config['data']['ref']
	output: 
		out = f"{wd}/ragoo_output/{out_name}.ragoo.fa"
	log:
		f"{wd}/logs/{out_name}.ragoo.log"	
	message:
		"Executing RaGOO for {input.assembly} assembly with {input.ref} as genome reference."
	singularity: 
		f"{path_to_sif}/RaGOO_1.1.sif"
	shell:
		"""
		ragoo.py {params.sv} {input.assembly} {input.ref}
		mv ragoo_output/ {wd}
		mv {wd}/ragoo_output/ragoo.scaffolds.fasta {output.out}
		"""

## SCAFFOLDING WITH RAGTAG ##
rule ragtag:
	input:
		assembly = config['data']['asm'],
		ref = config['data']['ref']
	output: 
		out = f"{wd}/ragtag_output/{out_name}.ragtag.fa"
	log:
		f"{wd}/logs/{out_name}.ragtag.log"	
	message:
		"Executing RagTag for {input.assembly} assembly with {input.ref} as genome reference."
	singularity: 
		f"{path_to_sif}/RagTag_v1.0.0.sif"	
	shell:
		"""
		ragtag.py correct {input.ref} {input.assembly} 2>{log}
		ragtag.py scaffold {input.ref} ragtag_output/{out_name}.corrected.fasta 2>>{log}
		mv ragtag_output/ {wd}
		mv {wd}/ragtag_output/ragtag.scaffolds.fasta {output.out}
		"""

## STATS SCAFFOLDED ASM ##
rule assemblystats:
	input:
		"{wd}/{scaffolding}_output/{out_name}.{scaffolding}.fa"	
	output:
		"{wd}/assembly_stats/{out_name}.{scaffolding}.fa.stats"
	params:
		format = "-s"
	log:
		#expand("{wd}/logs/{out_name}.{scaffolding}.assembly-stats.log", wd=wd, out_name=out_name, scaffolding=SCAFF_TOOL)
		"{wd}/logs/{out_name}.{scaffolding}.assembly-stats.log"
	message:
		"Executing assembly-stats for {input}."
	shell:
		"""
		assembly-stats {params.format} {input} 1>{output} 2>{log}
		"""

## ________________________________________ ##
## _________ Haplotype Separation _________ ##
## ________________________________________ ##
## WHATSHAP ##
rule whatshap_polyphase:
	input:
		bam = config['data']['bam'],
		vcf = config['data']['vcf'],
		ref = config['whatshap']['ref']
	output:
		#directory(f"{wd}/WhatsHap"),
		out = f"{wd}/WhatsHap/" + config['whatshap']['prefix'] + ".whatshap.phased.vcf.gz"
	params:
		ploidy = config['whatshap']['ploidy'],
		outname = config['whatshap']['prefix']+".whatshap.phased.vcf.gz",
	log:
		f"{wd}/logs/" + config['whatshap']['prefix'] + ".whatshap.log"
	message:
		"Executing WhatsHap"
	singularity:
		f"{path_to_sif}/whatshap_polyphase_0.18.sif"
	shell:
		"""
		whatshap polyphase -o {params.outname} -r {input.ref} -p {params.ploidy} {input.vcf} {input.bam} 2>{log}
		mv {params.outname} {output.out}
		"""

rule bcftools_consensus:
	input:
		pvcf = rules.whatshap_polyphase.output.out,
		ref = config['whatshap']['ref']
	output:
		expand("{wd}/WhatsHap/Haplotypes/{prefix}.whatshap.haplotype_{nb}.fasta", wd=wd, prefix=config['whatshap']['prefix'], nb=NB_HAPLOTYPES)
	params:
		ht_prefix = config['whatshap']['prefix']+".whatshap.haplotype",
		ploidy = config['whatshap']['ploidy'],
		ht = NB_HAPLOTYPES
	log:
		f"{wd}/logs/" + config['whatshap']['prefix'] + ".whatshap.bcftools.log"
	message:
		"Executing bcftools consensus for the phased vcf {input.pvcf} and {input.ref} genome reference, with a ploidy of " + config['whatshap']['ploidy']
	run:
		shell(f"bcftools index -f {input.pvcf}")
		for i in NB_HAPLOTYPES :
                        cmd = f"bcftools consensus -H " + i + f" -f {input.ref} {input.pvcf} > {wd}/WhatsHap/Haplotypes/{params.ht_prefix}_" + i + ".fasta"
			print(cmd)
			shell(cmd)

