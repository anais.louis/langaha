# Langaha

Snakemake workflow for reference-guided scaffolding and haplotype separation

---

## Requirements
- Singularity ≥ 3.3.0
- Python ≥ 3.6
- Snakemake ≥ 5.6.0
- Bcftools ≥ 1.10


## Usage

```
git clone https://gitlab.com/anais.louis/langaha.git
cd langaha
snakemake --verbose -s Snakefile --configfile config.yaml --use-singularity
```

You need to build **WhatsHap** image beforehand in order to use the phasing part of the workflow. In the ***tools*** directory do :
```
singularity build whatshap_polyphase_0.18.sif whatshap_polyphase.def
```



## Workflow

-- ONGOING --

![alt text](dag.svg "Scaffolding Workflow Overview")
![alt text](dag_phasing.svg "Phasing Workflow Overview")

## How to configure the pipeline ?

-- ONGOING --

### Requirements to use WhatsHap polyphase

In order to use WhatsHap polyphase, you need a reference for the polyploid genome from which you want to extract haplotypes. You first need to align your reads (short or long) to this genome and call variants with the appropriate ploidy. Then, you can use the reference genome, the vcf file and the bam file as inputs for WhatHap polyphase. A phased vcf will be created and will be used to extract each haplotypes from the reference genome with Bcftools consensus feature.

> *N.b. : it won't work if you don't have a reference genome.*

### Requirements to use RaGOO and RagTag

Both RaGOO and RagTag are reference-guided scaffolding tools. If you don't have a reference genome, you won't be able to use those tools to scaffold your assembly.


### Configuration file

Specify paths to results and singularity images directories :
```
paths:
        output_dir: "/path/to/results"
        singularity_imgs: "/path/to/langaha/tools"
```
> *N.b. : for now, you need to put the absolute path to directories (it will be updated soon).*  




Copy (or link) all required inputs to the ***data*** directory and specify files names : 
```
data:
        asm: "data/assembly.fa"
        ref: "data/reference.fa"
        bam: "data/readsVSreference.mapped.sort.bam" ## must be indexed (cf samtools index)
        vcf: "data/called_variants.vcf"
```

Specify which tools you want to use (True) :
```
scaffolding:
        ragoo: False
        ragtag: False

phasing:
        whatshap: True
```


Specify some options :
```
ragoo:
        call_sv: True

whatshap:
        ref: "/path/to/genome/used/to/call/variants/file.fa" ## need the .fai et .dict files (cf samtools faidx and samtools dict)
        ploidy: 3
        prefix: "prefix_for_output_files"

```

