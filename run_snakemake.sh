#!/bin/bash

#SBATCH --partition=normal
#SBATCH --nodelist=node23
#SBATCH --job-name=langaha
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1	## nb threads per task
#SBATCH -o langaha.o

wd="/scratch/alouis/langaha/"
cd $wd

module load system/singularity/3.3.0
module load bioinfo/assembly-stats/1.0.1
module load bioinfo/snakemake/5.9.1-conda

snakemake --cores 1 --unlock
snakemake --cores 1 --verbose --directory /scratch/alouis/langaha/results --use-singularity

